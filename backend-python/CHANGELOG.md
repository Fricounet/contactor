# Changelog

All notable changes to `Contactor backend` will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2022-06-30

### Added

- Bootstrap project using the [python-project-template](https://gitlab.com/Fricounet/python-project-template). [@Baptiste Girard-Carrabin](mailto:baptiste.girardcarrabin@datadoghq.com)
