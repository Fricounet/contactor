package main

import (
	"fmt"
	"time"
)

type Logger struct{}

var logger Logger

const (
	blue   = "\033[97;44m"
	yellow = "\033[90;43m"
	red    = "\033[97;41m"
	white  = "\033[90;47m"
	reset  = "\033[0m"
)

func levelColor(level string) string {
	switch level {
	case "INFO":
		return blue
	case "WARN":
		return yellow
	case "ERROR":
		return red
	default:
		return white
	}
}

func Log(level, message string) {
	fmt.Printf("[GIN] %v |%s %v %s| %v",
		time.Now().Format("2006/01/02 - 15:04:05"),
		levelColor(level), level, reset,
		message,
	)
}

func (l *Logger) Info(message string) {
	Log("INFO", message)
}

func (l *Logger) Warn(message string) {
	Log("WARN", message)
}

func (l *Logger) Error(message string) {
	Log("ERROR", message)
}
