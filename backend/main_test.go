package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

var router *gin.Engine
var beginDate = time.Date(2022, 3, 25, 0, 0, 0, 0, time.UTC)
var contact1 = contact{Id: 1, Name: "contact1", ContactInfo: "phone:06060606", Birthdate: time.Date(1978, 3, 13, 0, 0, 0, 0, time.UTC), Notes: "this is contact 1", Category: 1, LastContact: beginDate, NextContact: time.Date(2022, 4, 9, 0, 0, 0, 0, time.UTC), TriggerReminder: false}
var contact2 = contact{Id: 2, Name: "contact2", ContactInfo: "phone:16161616", Birthdate: time.Date(1995, 10, 23, 0, 0, 0, 0, time.UTC), Notes: "this is contact 2", Category: 2, LastContact: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC), NextContact: beginDate, TriggerReminder: true}

func UnmarshalJsonBody(res *httptest.ResponseRecorder) (result map[string]interface{}) {
	if err := json.Unmarshal(res.Body.Bytes(), &result); err != nil {
		log.Println(err)
	}
	return
}

func UnmarshalJsonContacts(res *httptest.ResponseRecorder) (result []contact) {
	if err := json.Unmarshal(res.Body.Bytes(), &result); err != nil {
		log.Println(err)
	}
	return
}

func UnmarshalJsonContact(res *httptest.ResponseRecorder) (result contact) {
	if err := json.Unmarshal(res.Body.Bytes(), &result); err != nil {
		log.Println(err)
	}
	return
}

func TestMain(m *testing.M) {
	testDb := sqlx.MustConnect("sqlite3", "contactor_test.db")
	defer testDb.Close()

	// trigger function to create schema
	createSchema(testDb)
	// we replace the DB pointer to the mock one
	db = testDb
	// init router
	router = setupRouter()
	// Run test suites
	exitVal := m.Run()
	// we can do clean up code here
	os.Exit(exitVal)
}

func createSchema(db *sqlx.DB) {
	query := `DROP TABLE IF EXISTS contacts;
	CREATE TABLE contacts (
		id INTEGER PRIMARY KEY,
		name TEXT NOT NULL UNIQUE,
		contact_info TEXT,
		birthdate DATE,
		notes TEXT,
		category INTEGER,
		last_contact DATE,
		next_contact DATE,
		trigger_reminder BOOLEAN
	);`
	if _, err := db.Exec(query); err != nil {
		log.Fatal(err)
	}
}

func createContacts(t *testing.T) {
	baseContacts := []contact{contact1, contact2}
	query := `INSERT INTO contacts(name, contact_info, birthdate, notes, category, last_contact, next_contact, trigger_reminder)
		VALUES(:name, :contact_info, :birthdate, :notes, :category, :last_contact, :next_contact, :trigger_reminder)`
	for _, contact := range baseContacts {
		if _, err := db.NamedExec(query, contact); err != nil {
			t.Error(fmt.Sprintln("could not populate database:", err))
		}
	}

	t.Cleanup(clearDb)
}

func clearDb() {
	query := "DELETE FROM contacts;"
	if _, err := db.Exec(query); err != nil {
		log.Fatal(fmt.Sprintln("could not clear database after test:", err))
	}
}

func TestGetHealthzRoute(t *testing.T) {
	// Test valid route
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/healthz", nil)
	router.ServeHTTP(w, req)
	expected := map[string]interface{}{"status": "UP"}

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, expected, UnmarshalJsonBody(w))
}

func TestGetContactsRoute(t *testing.T) {
	// Setup DB and teardown
	createContacts(t)

	// Test valid route
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/contacts", nil)
	router.ServeHTTP(w, req)
	expected := []contact{contact1, contact2}

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, expected, UnmarshalJsonContacts(w))
}

func TestGetContactRoute(t *testing.T) {
	// Setup DB and teardown
	createContacts(t)

	// Test valid route
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/contacts/1", nil)
	router.ServeHTTP(w, req)
	expected := contact1

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, expected, UnmarshalJsonContact(w))

	// Test bad param id
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/contacts/q", nil)
	router.ServeHTTP(w, req)
	expected_json := map[string]interface{}{"message": "bad param id"}

	assert.Equal(t, 400, w.Code)
	assert.Equal(t, expected_json, UnmarshalJsonBody(w))

	// Test contact not found
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/contacts/1000", nil)
	router.ServeHTTP(w, req)
	expected_json = map[string]interface{}{"message": "contact not found"}

	assert.Equal(t, 404, w.Code)
	assert.Equal(t, expected_json, UnmarshalJsonBody(w))
}

func TestAddContactRoute(t *testing.T) {
	// Setup DB and teardown
	createContacts(t)

	// Test valid route (test all fields to see if they are registered)
	newContact := contact{Name: "contact3", ContactInfo: "phone:36363636", Birthdate: time.Date(1978, 3, 13, 0, 0, 0, 0, time.UTC), Notes: "this is contact 3", Category: 3, LastContact: beginDate}
	body, _ := json.Marshal(&newContact)
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/contacts", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	expected := contact{Id: 3, Name: "contact3", ContactInfo: "phone:36363636", Birthdate: time.Date(1978, 3, 13, 0, 0, 0, 0, time.UTC), Notes: "this is contact 3", Category: 3, LastContact: beginDate, NextContact: time.Date(2022, 9, 21, 0, 0, 0, 0, time.UTC), TriggerReminder: false}

	assert.Equal(t, 201, w.Code)
	assert.Equal(t, expected, UnmarshalJsonContact(w))
	// check that the contact is stored correctly in the db
	storedContact, _ := getContactByField("id", 3)
	assert.Equal(t, expected, storedContact)

	// Test existing contact
	existingContact := contact{Id: 1, Name: "I already exist"}
	body, _ = json.Marshal(&existingContact)
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("POST", "/contacts", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	expected = contact{Id: 4, Name: "I already exist"}

	assert.Equal(t, 201, w.Code)
	assert.Equal(t, expected, UnmarshalJsonContact(w))
	// check that the contact is stored correctly in the db
	storedContact, _ = getContactByField("id", 4)
	assert.Equal(t, expected, storedContact)

	// Test missing name contact
	invalidContact := contact{}
	body, _ = json.Marshal(&invalidContact)
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("POST", "/contacts", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	expected_json := map[string]interface{}{"message": "wrong JSON body"}

	assert.Equal(t, 400, w.Code)
	assert.Equal(t, expected_json, UnmarshalJsonBody(w))

	// Test 500
	sameNameContact := contact{Name: "contact1"}
	body, _ = json.Marshal(&sameNameContact)
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("POST", "/contacts", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	expected_json = map[string]interface{}{"message": "something went wrong"}

	assert.Equal(t, 500, w.Code)
	assert.Equal(t, expected_json, UnmarshalJsonBody(w))
}

func TestUpdateContactRoute(t *testing.T) {
	// Setup DB and teardown
	createContacts(t)

	// Test valid route (all fields)
	newContact1 := contact{Id: 10, Name: "contact10", ContactInfo: "phone:07070707", Birthdate: time.Date(1984, 5, 15, 0, 0, 0, 0, time.UTC), Notes: "this is contact 10", Category: 3, LastContact: beginDate, TriggerReminder: true}
	body, _ := json.Marshal(&newContact1)
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PATCH", "/contacts/1", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	expected := contact{Id: 1, Name: "contact10", ContactInfo: "phone:07070707", Birthdate: time.Date(1984, 5, 15, 0, 0, 0, 0, time.UTC), Notes: "this is contact 10", Category: 3, LastContact: beginDate, NextContact: time.Date(2022, 9, 21, 0, 0, 0, 0, time.UTC), TriggerReminder: true}

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, expected, UnmarshalJsonContact(w))
	// check that the contact is stored correctly in the db
	storedContact, _ := getContactByField("id", 1)
	assert.Equal(t, expected, storedContact)

	// Test bad param id
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("PATCH", "/contacts/q", nil)
	router.ServeHTTP(w, req)
	expected_json := map[string]interface{}{"message": "bad param id"}

	assert.Equal(t, 400, w.Code)
	assert.Equal(t, expected_json, UnmarshalJsonBody(w))

	// Test contact not found
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("PATCH", "/contacts/1000", nil)
	router.ServeHTTP(w, req)
	expected_json = map[string]interface{}{"message": "contact not found"}

	assert.Equal(t, 404, w.Code)
	assert.Equal(t, expected_json, UnmarshalJsonBody(w))

	// Test wrong JSON body
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("PATCH", "/contacts/1", strings.NewReader("{\"category\":-1}"))
	router.ServeHTTP(w, req)
	expected_json = map[string]interface{}{"message": "wrong JSON body"}

	assert.Equal(t, 400, w.Code)
	assert.Equal(t, expected_json, UnmarshalJsonBody(w))

	// Test 500
	sameNameContact := contact{Name: "contact2"}
	body, _ = json.Marshal(&sameNameContact)
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("PATCH", "/contacts/1", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	expected_json = map[string]interface{}{"message": "something went wrong"}

	assert.Equal(t, 500, w.Code)
	assert.Equal(t, expected_json, UnmarshalJsonBody(w))
}

func TestComputeNextContact(t *testing.T) {
	params := []struct {
		category    int
		lastContact time.Time
		nextContact time.Time
		err         error
	}{
		{category: 0, lastContact: beginDate, nextContact: time.Time{}, err: nil},
		{category: 1, lastContact: beginDate, nextContact: time.Date(2022, 4, 9, 0, 0, 0, 0, time.UTC), err: nil},
		{category: 2, lastContact: beginDate, nextContact: time.Date(2022, 5, 14, 0, 0, 0, 0, time.UTC), err: nil},
		{category: 3, lastContact: beginDate, nextContact: time.Date(2022, 9, 21, 0, 0, 0, 0, time.UTC), err: nil},
		{category: 4, lastContact: beginDate, nextContact: time.Date(2023, 3, 25, 0, 0, 0, 0, time.UTC), err: nil},
		{category: 5, lastContact: beginDate, nextContact: time.Time{}, err: errors.New("category 5 is not supported")},
	}

	for _, p := range params {
		contact := contact{Category: p.category, LastContact: p.lastContact}
		err := contact.computeNextContact()

		assert.Equal(t, p.nextContact, contact.NextContact)
		assert.Equal(t, p.err, err)
	}
}

func TestGetContactByField(t *testing.T) {
	// Setup DB and teardown
	createContacts(t)

	// Test valid searches
	expectedContact := contact1
	params := []struct {
		field string
		value interface{}
	}{
		{field: "id", value: 1},
		{field: "name", value: "contact1"},
		{field: "contact_info", value: "phone:06060606"},
		{field: "birthdate", value: time.Date(1978, 3, 13, 0, 0, 0, 0, time.UTC)},
		{field: "notes", value: "this is contact 1"},
		{field: "category", value: 1},
		{field: "last_contact", value: beginDate},
		{field: "next_contact", value: time.Date(2022, 4, 9, 0, 0, 0, 0, time.UTC)},
		{field: "trigger_reminder", value: false},
	}

	for _, p := range params {
		contact, err := getContactByField(p.field, p.value)

		assert.Equal(t, expectedContact, contact)
		assert.Equal(t, nil, err)
	}

	// Test empty result
	var emptyContact contact
	contact, err := getContactByField("id", 5)

	assert.Equal(t, emptyContact, contact)
	assert.Equal(t, errors.New("sql: no rows in result set"), err)
}

func TestAuthentication(t *testing.T) {
	viper.Set("username", "test")
	viper.Set("password", "123456")
	router = setupRouter()

	// Test health route
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/healthz", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)

	// Test unauthenticated
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/contacts", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 401, w.Code)

	// Test authenticated
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/contacts", nil)
	req.SetBasicAuth("test", "123456")
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
}
