package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"github.com/spf13/viper"
)

var CATEGORIES = map[int]int{ // Map the category to a number of day between each time I contact
	0: 0,   // Undefined
	1: 15,  // Every 15 days
	2: 50,  // Every 2 months
	3: 180, // Every 6 months
	4: 365, // Every year
}

type contact struct {
	Id              int       `json:"id"`
	Name            string    `json:"name" binding:"required"`
	ContactInfo     string    `json:"contactInfo" db:"contact_info"`
	Birthdate       time.Time `json:"birthdate"`
	Notes           string    `json:"notes"`
	Category        int       `json:"category" binding:"min=0,max=4"`
	LastContact     time.Time `json:"lastContact" db:"last_contact" time_format:"2006-01-02"`
	NextContact     time.Time `json:"nextContact" db:"next_contact" time_format:"2006-01-02"`
	TriggerReminder bool      `json:"triggerReminder" db:"trigger_reminder"`
}

func (c *contact) computeNextContact() error {
	// Update nextContact depending on the category
	delayInDays, ok := CATEGORIES[c.Category]
	if ok {
		if c.Category > 0 { // Do not add nextContact for category 0
			c.NextContact = c.LastContact.AddDate(0, 0, delayInDays)
		} else {
			logger.Info(fmt.Sprintln("no nextContact set for contact:", c.Name))
		}
		return nil
	} else {
		return fmt.Errorf("category %v is not supported", c.Category)
	}
}

var db *sqlx.DB
var err error

func getContacts(ctx *gin.Context) {
	var contacts []contact
	query := "select * from contacts"
	err = db.Select(&contacts, query)
	if err != nil {
		logger.Error(fmt.Sprintf("unable to fetch contacts. Query: %v. Error: %v", query, err))
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": "something went wrong"})
		return
	}
	ctx.IndentedJSON(http.StatusOK, contacts)
}

func getContact(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		logger.Error(fmt.Sprintf("unable to convert id param %v to int. Error: %v", id, err))
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "bad param id"})
		return
	}
	contact, err := getContactByField("id", id)
	if err != nil {
		logger.Error(fmt.Sprintf("unable to find contact with id %v. Error: %v", id, err))
		ctx.JSON(http.StatusNotFound, gin.H{"message": "contact not found"})
		return
	}
	ctx.IndentedJSON(http.StatusOK, contact)
}

func addContact(ctx *gin.Context) {
	var newContact contact
	if err := ctx.ShouldBindJSON(&newContact); err != nil {
		logger.Error(fmt.Sprintln("unable to bind newContact:", err))
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "wrong JSON body"})
		return
	}

	if err = newContact.computeNextContact(); err != nil {
		// Should never happen because of the binding on Category
		logger.Error(err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "wrong JSON body"})
		return
	}

	query := `INSERT INTO contacts(name, contact_info, birthdate, notes, category, last_contact, next_contact, trigger_reminder)
          VALUES(:name, :contact_info, :birthdate, :notes, :category, :last_contact, :next_contact, :trigger_reminder)`
	res, err := db.NamedExec(query, newContact)
	if err != nil {
		logger.Error(fmt.Sprintf("unable to create contact. Query: %v. Error: %v", query, err))
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": "something went wrong"})
		return
	}
	contactId, _ := res.LastInsertId()
	newContact.Id = int(contactId)
	ctx.IndentedJSON(http.StatusCreated, newContact)
}

func updateContact(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		logger.Error(fmt.Sprintf("unable to convert id param %v to int. Error: %v", id, err))
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "bad param id"})
		return
	}

	updatedContact, err := getContactByField("id", id)
	if err != nil {
		logger.Error(fmt.Sprintf("unable to find contact with id %v. Error: %v", id, err))
		ctx.JSON(http.StatusNotFound, gin.H{"message": "contact not found"})
		return
	}

	if err := ctx.ShouldBindJSON(&updatedContact); err != nil {
		logger.Error(fmt.Sprintln("unable to bind updatedContact:", err))
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "wrong JSON body"})
		return
	}

	// The id will not be changed by the db query so show
	// its right value in the http response
	updatedContact.Id = id

	if err = updatedContact.computeNextContact(); err != nil {
		// Should never happen because of the binding on Category
		logger.Error(err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "wrong JSON body"})
		return
	}

	query := fmt.Sprintf(`UPDATE contacts SET name=:name, contact_info=:contact_info, birthdate=:birthdate, notes=:notes,
	  category=:category, last_contact=:last_contact, next_contact=:next_contact, trigger_reminder=:trigger_reminder
	  WHERE id=%v`, id)
	_, err = db.NamedExec(query, updatedContact)
	if err != nil {
		logger.Error(fmt.Sprintf("unable to update contact. Query: %v. Error: %v", query, err))
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": "something went wrong"})
		return
	}
	ctx.IndentedJSON(http.StatusOK, updatedContact)
}

func getContactByField(field string, value interface{}) (contact, error) {
	var contact contact
	query := fmt.Sprintf("select * from contacts where %v = ?", field)
	err = db.Get(&contact, query, value)
	return contact, err
}

func setDefaultConfig() {
	viper.SetDefault("username", "")
	viper.SetDefault("password", "")
	viper.SetDefault("dbName", "contactor.db")
	viper.SetDefault("dbEngine", "sqlite3")
}

func setupRouter() *gin.Engine {
	var router = gin.Default()
	// If you are behind a proxy: router.SetTrustedProxies([]string{"name or ip"})
	router.SetTrustedProxies(nil)

	// Enable basic auth if username is set
	var contactor *gin.RouterGroup
	if viper.GetString("username") != "" {
		contactor = router.Group("/", gin.BasicAuth(gin.Accounts{
			viper.GetString("username"): viper.GetString("password"),
		}))
	} else {
		contactor = router.Group("/")
	}

	// Do not hide healthcheck behind password
	router.GET("/healthz", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{"status": "UP"})
	})
	contactor.GET("/contacts", getContacts)
	contactor.GET("/contacts/:id", getContact)
	contactor.POST("/contacts", addContact)
	contactor.PATCH("/contacts/:id", updateContact)

	return router
}

func main() {
	// Init config
	viper.SetConfigName("config") // name of config file (without extension)
	viper.AddConfigPath(".")      // look for config in the working directory
	setDefaultConfig()            // set default config before overriding with true config
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			logger.Warn("config file not found, default config will be used.")

		} else {
			log.Fatal(err)
		}
	}

	// Init DB
	db, err = sqlx.Connect(viper.GetString("dbEngine"), viper.GetString("dbName"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	router := setupRouter()
	router.Run("localhost:4000")
}
