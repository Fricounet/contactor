DROP TABLE IF EXISTS contacts;
CREATE TABLE contacts (
    id        INTEGER PRIMARY KEY,
    name      TEXT NOT NULL UNIQUE,
    contact_info TEXT,
    birthdate DATE,
    notes TEXT,
    category INTEGER,
    last_contact DATE,
    next_contact DATE,
    trigger_reminder BOOLEAN
);

INSERT INTO contacts
    (name, contact_info, birthdate, notes, category, last_contact, next_contact, trigger_reminder)
    VALUES
        ('maman', 'phone:0606060', date('now', '-50 years'), 'c ma maman', 1, date('now'), date('now','+21 days'), false),
        ('papa', 'phone:0616161', date('now', '-63 years'), 'c mon papa', 1, date('now', '-1 month'), date('now'), true),
        ('jean', 'phone:0626262', date('now', '-15 years'), 'c mon jean', 2, date('now', '-1 month'), date('now','+1 month'), false);
